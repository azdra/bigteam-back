const strapi = require('strapi')
const fs = require('fs');
const chalk = require('chalk');

const importData = {'project': 100}

strapi().load().then( async (strapiInstance) => {
  const path = __dirname+"/ImportData";

  console.log(chalk.green('Start import'));
  const data = [];
  const files = await fs.readdirSync(path);
  if (!files) return;

  for (const file of files)
    data[importData[file]] = {
      name: file,
      data: JSON.parse(await fs.readFileSync(path+"/"+file, 'utf-8'))
    };

  data.sort();

  for (const iData of data) {
    if (!iData) continue;
    console.log(chalk.cyan('Import %s'), iData.name);

    for (const iDataElementElement of iData['data'])
      await strapiInstance.query(iData['name']).create({
        name: iDataElementElement['title'],
        deadLine: new Date(new Date().getTime()+8.64e+7),
      })
  }

  console.log(chalk.green('End import'));
  strapiInstance.stop(0)
})

